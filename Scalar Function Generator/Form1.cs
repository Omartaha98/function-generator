﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;

namespace Scalar_Function_Generator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            XmlDocument doc = new XmlDocument();
            doc.Load("Data.xml"); //("/Names/Name[@type='M']");
            XmlNodeList tables = doc.GetElementsByTagName("Table");

            // Console.WriteLine(tables.Count);
            for (int i = 0; i < tables.Count; i++)
            {
                string name = tables[i].Attributes["Name"].Value;
                comboBox4.Items.Add(name);
            }
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }
        public void Summation(int index,string colName)
        {

            int sum = 0;
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                sum += Convert.ToInt32(dataGridView1.Rows[i].Cells[index].Value);
            }
            MessageBox.Show("The Sum of "+colName+" is : "+sum.ToString());
        }
        public void Average(int index,string colName)
        {
            int sum = 0;
            double avg = 0;
            for (int i = 0; i < dataGridView1.Rows.Count; ++i)
            {
                sum += Convert.ToInt32(dataGridView1.Rows[i].Cells[index].Value);
            }
            avg = sum / dataGridView1.Rows.Count;
            MessageBox.Show("The Average of " + colName + " is : " + avg.ToString());
        }
        public void Get_Max(int index,string colName){
            int high = 0, highest = 0;
            highest = Convert.ToInt32(dataGridView1.Rows[0].Cells[index].Value);
            for (int i = 0; i < dataGridView1.Rows.Count; ++i)
            {
                high = Convert.ToInt32(dataGridView1.Rows[i].Cells[index].Value);
                if (high > highest)
                {
                    highest = high;
                }

            }
            MessageBox.Show("The Maximum " + colName + " is : " + highest.ToString());
        }
        public void Get_Min(int index,string colName)
        {
            int low = 99999, lowest = 99999;
            lowest = Convert.ToInt32(dataGridView1.Rows[0].Cells[index].Value);
            for (int i = 0; i < dataGridView1.Rows.Count - 1; ++i)
            {
                low = Convert.ToInt32(dataGridView1.Rows[i].Cells[index].Value);
                if (low < lowest)
                {
                    lowest = low;
                }

            }
            MessageBox.Show("The Minimum " + colName + " is : " + lowest.ToString());
        }
        public void Get_Count(int index,string colName)
        {
           string d = textBox1.Text;
            int count = 0;
            for (int i = 0; i < dataGridView1.Rows.Count-1; i++)
            {
                string r = dataGridView1.Rows[i].Cells[index].Value.ToString();
                
                Console.WriteLine(textBox1.Text);
                if (d == r)
                {
                    count++;
                }


            }
            MessageBox.Show("The Count of " + colName + " is : " + count.ToString());
        
        }
        public void Above(int index)
        {
            dataGridView2.Rows.Clear();
            dataGridView2.Columns.Clear();
            dataGridView2.Visible = false;
            string RowName = comboBox4.SelectedItem.ToString();

            XmlDocument doc = new XmlDocument();
            doc.Load("Data.xml"); //("/Names/Name[@type='M']");
            string path = "/Tables/" + "Table" + "/Row" + "[@Name=\"" + RowName + "\"]";
            XmlNodeList list1 = doc.SelectNodes(path);
            for (int i = 0; i < list1.Count; i++)
            {
                XmlNodeList childrens = list1[i].ChildNodes;
                string id = childrens[0].Attributes["Name"].Value;

                string name = childrens[1].Attributes["Name"].Value;

                string gender = childrens[2].Attributes["Name"].Value;
                
                string salary = childrens[3].Attributes["Name"].Value;

                string depart = childrens[4].Attributes["Name"].Value;

                if (dataGridView2.ColumnCount == 0)
                {
                    dataGridView2.Columns.Add(id, id);
                    dataGridView2.Columns.Add(name, name);
                    dataGridView2.Columns.Add(gender, gender);
                    dataGridView2.Columns.Add(salary, salary);
                    dataGridView2.Columns.Add(depart, depart);
                }

            }
            string ivalue=null, nvalue=null, gendervalue=null, salaryvalue=null, departvalue=null;
            int d = Convert.ToInt32(textBox1.Text);
            for (int i = 0; i < dataGridView1.Rows.Count - 1; ++i)
            {
                if (Convert.ToInt32(dataGridView1.Rows[i].Cells[index].Value) > d)
                {
                     ivalue = dataGridView1.Rows[i].Cells[0].Value.ToString();
                     nvalue = dataGridView1.Rows[i].Cells[1].Value.ToString();
                     gendervalue = dataGridView1.Rows[i].Cells[2].Value.ToString();
                     salaryvalue = dataGridView1.Rows[i].Cells[3].Value.ToString();
                     departvalue = dataGridView1.Rows[i].Cells[4].Value.ToString();
                dataGridView2.Rows.Add(new string[] { ivalue, nvalue, gendervalue, salaryvalue, departvalue });
                }
            }
           
                dataGridView2.Visible = true;
        }
        public void Below(int index)
        {
            dataGridView2.Rows.Clear();
            dataGridView2.Columns.Clear();
            dataGridView2.Visible = false;
            string RowName = comboBox4.SelectedItem.ToString();

            XmlDocument doc = new XmlDocument();
            doc.Load("Data.xml"); //("/Names/Name[@type='M']");
            string path = "/Tables/" + "Table" + "/Row" + "[@Name=\"" + RowName + "\"]";
            XmlNodeList list1 = doc.SelectNodes(path);
            for (int i = 0; i < list1.Count; i++)
            {
                XmlNodeList childrens = list1[i].ChildNodes;
                string id = childrens[0].Attributes["Name"].Value;

                string name = childrens[1].Attributes["Name"].Value;

                string gender = childrens[2].Attributes["Name"].Value;

                string salary = childrens[3].Attributes["Name"].Value;

                string depart = childrens[4].Attributes["Name"].Value;

                if (dataGridView2.ColumnCount == 0)
                {
                    dataGridView2.Columns.Add(id, id);
                    dataGridView2.Columns.Add(name, name);
                    dataGridView2.Columns.Add(gender, gender);
                    dataGridView2.Columns.Add(salary, salary);
                    dataGridView2.Columns.Add(depart, depart);
                }

            }
            string ivalue = null, nvalue = null, gendervalue = null, salaryvalue = null, departvalue = null;
            int d = Convert.ToInt32(textBox1.Text);
            for (int i = 0; i < dataGridView1.Rows.Count - 1; ++i)
            {
                if (Convert.ToInt32(dataGridView1.Rows[i].Cells[index].Value) < d)
                {
                    ivalue = dataGridView1.Rows[i].Cells[0].Value.ToString();
                    nvalue = dataGridView1.Rows[i].Cells[1].Value.ToString();
                    gendervalue = dataGridView1.Rows[i].Cells[2].Value.ToString();
                    salaryvalue = dataGridView1.Rows[i].Cells[3].Value.ToString();
                    departvalue = dataGridView1.Rows[i].Cells[4].Value.ToString();
                    dataGridView2.Rows.Add(new string[] { ivalue, nvalue, gendervalue, salaryvalue, departvalue });
                }
            }

            dataGridView2.Visible = true;
        }
        public void Show_Has(int index)
        {
            dataGridView2.Rows.Clear();
            dataGridView2.Columns.Clear();
            dataGridView2.Visible = false;
            string RowName = comboBox4.SelectedItem.ToString();

            XmlDocument doc = new XmlDocument();
            doc.Load("Data.xml"); //("/Names/Name[@type='M']");
            string path = "/Tables/" + "Table" + "/Row" + "[@Name=\"" + RowName + "\"]";
            XmlNodeList list1 = doc.SelectNodes(path);
            for (int i = 0; i < list1.Count; i++)
            {
                XmlNodeList childrens = list1[i].ChildNodes;
                string id = childrens[0].Attributes["Name"].Value;

                string name = childrens[1].Attributes["Name"].Value;

                string gender = childrens[2].Attributes["Name"].Value;

                string salary = childrens[3].Attributes["Name"].Value;

                string depart = childrens[4].Attributes["Name"].Value;

                if (dataGridView2.ColumnCount == 0)
                {
                    dataGridView2.Columns.Add(id, id);
                    dataGridView2.Columns.Add(name, name);
                    dataGridView2.Columns.Add(gender, gender);
                    dataGridView2.Columns.Add(salary, salary);
                    dataGridView2.Columns.Add(depart, depart);
                }

            }
            string ivalue = null, nvalue = null, gendervalue = null, salaryvalue = null, departvalue = null;
            string d = textBox1.Text;
            for (int i = 0; i < dataGridView1.Rows.Count - 1; ++i)
            {
                if (dataGridView1.Rows[i].Cells[index].Value.ToString() == d)
                {
                    ivalue = dataGridView1.Rows[i].Cells[0].Value.ToString();
                    nvalue = dataGridView1.Rows[i].Cells[1].Value.ToString();
                    gendervalue = dataGridView1.Rows[i].Cells[2].Value.ToString();
                    salaryvalue = dataGridView1.Rows[i].Cells[3].Value.ToString();
                    departvalue = dataGridView1.Rows[i].Cells[4].Value.ToString();
                    dataGridView2.Rows.Add(new string[] { ivalue, nvalue, gendervalue, salaryvalue, departvalue });
                }
            }

            dataGridView2.Visible = true;
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridView2.Visible = false;
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();
            comboBox1.Items.Clear();
            string RowName = comboBox4.SelectedItem.ToString();

            XmlDocument doc = new XmlDocument();
            doc.Load("Data.xml"); //("/Names/Name[@type='M']");
            string path = "/Tables/" + "Table" + "/Row" + "[@Name=\"" + RowName + "\"]";
            XmlNodeList list1 = doc.SelectNodes(path);
            for (int i = 0; i < list1.Count; i++)
            {
                XmlNodeList childrens = list1[i].ChildNodes;
                string id = childrens[0].Attributes["Name"].Value;
                string ivalue = childrens[0].InnerText;

                string name = childrens[1].Attributes["Name"].Value;
                string nvalue = childrens[1].InnerText;

                string gender = childrens[2].Attributes["Name"].Value;
                string gendervalue = childrens[2].InnerText;

                string salary = childrens[3].Attributes["Name"].Value;
                string salaryvalue = childrens[3].InnerText;

                string depart = childrens[4].Attributes["Name"].Value;
                string departvalue = childrens[4].InnerText;
                if(i==0)
                {
                    comboBox1.Items.Add(id);
                    comboBox1.Items.Add(name);
                    comboBox1.Items.Add(gender);
                    comboBox1.Items.Add(salary);
                    comboBox1.Items.Add(depart);
                }

                if (dataGridView1.ColumnCount == 0)
                {
                    dataGridView1.Columns.Add(id, id);
                    dataGridView1.Columns.Add(name, name);
                    dataGridView1.Columns.Add(gender, gender);
                    dataGridView1.Columns.Add(salary, salary);
                    dataGridView1.Columns.Add(depart, depart);
                }
                dataGridView1.Rows.Add(new string[] { ivalue, nvalue, gendervalue, salaryvalue, departvalue });
            
            }
            
        }

      

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBox6.Items.Clear();
            string RowName = comboBox4.SelectedItem.ToString();
            string colName = comboBox1.SelectedItem.ToString();
            int ind = dataGridView1.Columns[colName].Index;
            XmlDocument docs = new XmlDocument();
            docs.Load("Data.xml");
            string path = "/Tables/" + "Table" + "/Row" + "[@Name=\"" + RowName + "\"]";
            XmlNodeList list1 = docs.SelectNodes(path);
            XmlNodeList child = list1[0].ChildNodes;
            string colType = child[ind].Attributes["Type"].Value;
            
            XmlDocument doc = new XmlDocument();
            doc.Load("Function.xml"); //("/Names/Name[@type='M']");
            XmlNodeList tables = doc.GetElementsByTagName("Function");


            // Console.WriteLine(tables.Count);
            for (int i = 0; i < tables.Count; i++)
            {
             
                XmlNodeList children = tables[i].ChildNodes;
                string name = children[0].InnerText;
                string funcType = children[1].InnerText;
                if (funcType == colType || funcType == "var")
                {
                    comboBox6.Items.Add(name);
                }
                
            }
            
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            /*int ind = dataGridView1.Columns[comboBox1.SelectedText].Index;
            //int col = dataGridView1.;
            MessageBox.Show(ind.ToString());*/
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string colName = comboBox1.SelectedItem.ToString();
            int ind = dataGridView1.Columns[colName].Index;

            if (comboBox6.SelectedItem.ToString() == "SUM")
            {
                Summation(ind, colName);
            }
            if (comboBox6.SelectedItem.ToString() == "AVG")
            {
                Average(ind, colName);
            }
            if (comboBox6.SelectedItem.ToString() == "MAX")
            {
                Get_Max(ind, colName);
            }
            if (comboBox6.SelectedItem.ToString() == "MIN")
            {
                Get_Min(ind, colName);
            }
            if (comboBox6.SelectedItem.ToString() == "COUNT")
            {
                Get_Count(ind, colName);
            }
            if (comboBox6.SelectedItem.ToString() == "ABOVE")
            {
                Above(ind);
            }
            if (comboBox6.SelectedItem.ToString() == "BELOW")
            {
                Below(ind);
            }
            if (comboBox6.SelectedItem.ToString() == "SHOW HAS")
            {
                Show_Has(ind);
            }


        }

       

    }
}
